<html>

<?php
    header('part-two: 09919d21');
?>

<head>
    <!-- part-one: aeb9573c -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/default.css">
    <title>xuseful</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
</head>

<body>
    <h1>xuseful is a game</h1>
    <p>this is your <b>second</b> clue.</p>
    <p class="clue">9 20 18 19 20 18 4 24 14 20 17 7 4 0 3 18</p>
</body>

</html>
    